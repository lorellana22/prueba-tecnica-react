import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate,
} from "react-router-dom";
import React from "react";
import UserDetailsPage from "./UserDetailsPage";
import UsersListPage from "./UsersListPage";
import UsersCreationPage from "./UsersCreationPage";

const App = () => (
  <Router>
    <Routes>
      <Route exact path="/users/:id" element={<UserDetailsPage />} />
      <Route exact path="/users" element={<UsersListPage />} />
      <Route exact path="/create-user" element={<UsersCreationPage />} />
      <Route exact path="/" element={<Navigate to="/users" />} />
    </Routes>
  </Router>
);

export default App;
