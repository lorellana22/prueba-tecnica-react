import PropTypes from "prop-types";
import { useNavigate } from "react-router-dom";
import React from "react";
import AppBar from "@mui/material/AppBar";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import IconButton from "@mui/material/IconButton";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { indigo, red } from "@mui/material/colors";
import CssBaseline from "@mui/material/CssBaseline";

const darkThemeConfig = {
  palette: {
    type: "dark",
    primary: {
      main: indigo.A100,
    },
    error: {
      main: red.A100,
    },
  },
};

const Page = ({ children, title, actionButtons, goBackLink }) => {
  const navigate = useNavigate();
  const darkTheme = createTheme(darkThemeConfig);

  return (
    <ThemeProvider theme={darkTheme}>
      <CssBaseline />
      <AppBar position="sticky">
        <Toolbar>
          {goBackLink && (
            <IconButton
              color="inherit"
              edge="start"
              onClick={() => navigate("/users", { replace: goBackLink })}
            >
              <ArrowBackIcon />
            </IconButton>
          )}
          <Box flexGrow={1}>
            <Typography variant="h6">{title}</Typography>
          </Box>
          {actionButtons &&
            actionButtons.map((button) => (
              <IconButton
                key={button.id}
                color="inherit"
                onClick={button.onClick}
              >
                {button.icon}
              </IconButton>
            ))}
        </Toolbar>
      </AppBar>
      <Container>{children}</Container>
    </ThemeProvider>
  );
};

Page.defaultProps = {
  showGoBack: false,
};

Page.propTypes = {
  children: PropTypes.node.isRequired,
  title: PropTypes.string,
  actionButtons: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      icon: PropTypes.node.isRequired,
      onClick: PropTypes.func.isRequired,
    })
  ),
  goBackLink: PropTypes.string,
};

export default Page;
