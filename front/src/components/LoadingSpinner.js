import React from "react";
import Box from '@mui/material/Box';
import CircularProgress from '@mui/material/CircularProgress';


const LoadingSpinner = () => (
    <Box
    mt={5}
    display="flex"
    justifyContent="center">
    <CircularProgress />
    </Box>
    );

export default LoadingSpinner;