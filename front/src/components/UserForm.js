import PropTypes from "prop-types";
import React from "react";
import Box from "@mui/material/Box";
import FormControl from "@mui/material/FormControl";
import TextField from "@mui/material/TextField";

const UserForm = ({ disabled, value, onChange }) => {
  const getValidationErrors = (v) => {
    const requiredText = "Requerido";
    const nameLength = v.name.trim().length;
    return {
      name:
        v.name.length === 0
          ? requiredText
          : nameLength < 3
          ? "Se requieren al menos 3 caracteres"
          : "",
      lastName: v.lastName ? "" : requiredText,
      age: v.age ? "" : requiredText,
      email: v.email.match(/^[.\d\w\-_]+?@[\d\w\-_]+?\.[\w]+$/)
        ? ""
        : "Debe ser un email",
    };
  };
  const isValid = (v) => {
    const e = getValidationErrors(v);
    return Object.keys(e).every((field) => !e[field]);
  };
  const errors = getValidationErrors(value);

  const fieldChangeHandler = (fieldName) => {
    return (event) => {
      const newValue = {
        ...value,
        [fieldName]: event.target.value,
      };
      onChange({
        value: newValue,
        isValid: isValid(newValue),
      });
    };
  };

  return (
    <Box maxWidth={350} mx="auto" my={2}>
      <FormControl fullWidth margin="normal">
        <TextField
          label="Nombre"
          disabled={disabled}
          value={value.name}
          onChange={fieldChangeHandler("name")}
          error={!!errors.name}
          helperText={errors.name}
        />
      </FormControl>
      <FormControl fullWidth margin="normal">
        <TextField
          label="Apellido"
          disabled={disabled}
          value={value.lastName}
          onChange={fieldChangeHandler("lastName")}
          error={!!errors.lastName}
          helperText={errors.lastName}
        />
      </FormControl>
      <FormControl fullWidth margin="normal">
        <TextField
          label="Edad"
          type="number"
          disabled={disabled}
          value={value.age}
          onChange={fieldChangeHandler("age")}
          error={!!errors.age}
          helperText={errors.age}
        />
      </FormControl>
      <FormControl fullWidth margin="normal">
        <TextField
          label="Email"
          type="email"
          value={value.email}
          disabled={disabled}
          onChange={fieldChangeHandler("email")}
          error={!!errors.email}
          helperText={errors.email}
        />
      </FormControl>
    </Box>
  );
};

UserForm.propTypes = {
  value: PropTypes.shape({
    name: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    age: PropTypes.number.isRequired,
    email: PropTypes.string.isRequired,
  }).isRequired,
  onChange: PropTypes.func.isRequired,
};

export default UserForm;
