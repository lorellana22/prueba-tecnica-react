import { useEffect, useState } from "react";
import React from "react";
import { useNavigate, useParams } from "react-router-dom";
import UsersListService from "../services/UsersService";
import UserForm from "./UserForm";
import ErrorAlert from "./ErrorAlert";
import LoadingSpinner from "./LoadingSpinner";
import ConfirmDialog from "./ConfirmDialog";
import Page from "./Page";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";

const UiState = {
  None: 0,
  Ready: 1,
  Loading: 2,
  ErrorLoading: 3,
  Processing: 4,
  ErrorProcessing: 5,
  DeleteConfirmation: 6,
};

const UserDetailsPage = () => {
  let id = useParams();
  const [user, setUser] = useState();
  const [title, setTitle] = useState();
  const [uiState, setUiState] = useState(UiState.None);
  const [isValid, setIsValid] = useState(true);
  const navigate = useNavigate();

  const fetchUser = async (id) => {
    setUiState(UiState.Loading);
    setTitle("Cargando...");
    try {
      const res = await UsersListService.get(id);
      setUser(res.data);
      setUiState(UiState.Ready);
      setTitle(`${res.data.name} ${res.data.lastName}`);
    } catch {
      setUiState(UiState.ErrorLoading);
      setTitle("Error");
    }
  };

  const deleteUser = async () => {
    setUiState(UiState.Processing);
    try {
      await UsersListService.delete(user);
      navigate(`/users/`, { replace: true });
    } catch {
      setUiState(UiState.ErrorProcessing);
    }
  };

  const updateUser = async () => {
    setUiState(UiState.Processing);
    try {
      await UsersListService.update(user);
      setUiState(UiState.Ready);
      setTitle(user.name);
    } catch {
      setUiState(UiState.ErrorProcessing);
    }
  };

  const userChangeHandler = (event) => {
    setUser(event.value);
    setIsValid(event.isValid);
  };

  const deleteClickHandler = () => {
    setUiState(UiState.DeleteConfirmation);
  };

  const updateClickHandler = () => {
    updateUser();
  };

  const confirmDialogCloseHandler = (isConfirm) => {
    setUiState(UiState.Ready);
    if (isConfirm) {
      deleteUser();
    }
  };

  useEffect(() => {
    fetchUser(id);
  }, [id]);

  const isUserFormVisible = [
    UiState.Ready,
    UiState.Processing,
    UiState.ErrorProcessing,
  ].includes(uiState);

  const isSpinnerVisible = [UiState.Loading, UiState.Processing].includes(
    uiState
  );

  const isFormDisabled = uiState === UiState.Processing;

  return (
    <Page title={title} goBackLink="/users">
      {uiState === UiState.ErrorLoading && (
        <ErrorAlert
          message={"Error cargando los detalles del usuario"}
          onRetryClick={() => fetchUser(id)}
        />
      )}
      {isUserFormVisible && (
        <Box maxWidth={350} mx="auto" my={2}>
          <UserForm
            disabled={isFormDisabled}
            value={user}
            onChange={userChangeHandler}
          />
          <Box display="flex" justifyContent="space-between" mt={2}>
            <Button
              color="secondary"
              variant="contained"
              disabled={isFormDisabled}
              onClick={deleteClickHandler}
            >
              Eliminar
            </Button>
            <Button
              color="primary"
              variant="contained"
              disabled={isFormDisabled || !isValid}
              onClick={updateClickHandler}
            >
              Actualizar
            </Button>
          </Box>
        </Box>
      )}
      {isSpinnerVisible && <LoadingSpinner />}
      <ConfirmDialog
        open={uiState === UiState.DeleteConfirmation}
        title="Confirmar eliminación"
        message="¿Seguro que desea eliminar este usuario?"
        onClose={confirmDialogCloseHandler}
      />
    </Page>
  );
};

export default UserDetailsPage;
