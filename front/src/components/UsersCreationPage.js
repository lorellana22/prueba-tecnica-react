import { useState } from "react";
import { useNavigate } from "react-router-dom";
import React from "react";
import UsersListService from "../services/UsersService";
import LoadingSpinner from "./LoadingSpinner";
import Page from "./Page";
import UserForm from "./UserForm";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";

const UiState = {
  Ready: 1,
  Processing: 2,
  ErrorProcessing: 3,
};

const emptyUser = {
  name: "",
  lastName: "",
  age: 0,
  email: "",
};

const UsersCreationPage = () => {
  const navigate = useNavigate();
  const [user, setUser] = useState(emptyUser);
  const [isValid, setIsValid] = useState(false);
  const [uiState, setUiState] = useState(UiState.Ready);

  const createUser = async () => {
    setUiState(UiState.Processing);
    try {
      const res = await UsersListService.create(user);
      navigate(`/users/${res.data.id}`, { replace: true });
    } catch {
      setUiState(UiState.ErrorProcessing);
    }
  };

  const userChangeHandler = (event) => {
    setUser(event.value);
    setIsValid(event.isValid);
  };

  const createClickHandler = () => {
    createUser();
  };

  const isProcessing = uiState === UiState.Processing;

  return (
    <Page title="Nuevo usuario" goBackLink="/users">
      <Box maxWidth={350} mx="auto" my={2}>
        <UserForm
          disabled={isProcessing}
          value={user}
          onChange={userChangeHandler}
        />
        <Box display="flex" justifyContent="flex-end" mt={2}>
          <Button
            color="primary"
            variant="contained"
            disabled={isProcessing || !isValid}
            onClick={createClickHandler}
          >
            Crear
          </Button>
        </Box>
        {isProcessing && <LoadingSpinner />}
      </Box>
    </Page>
  );
};

export default UsersCreationPage;
