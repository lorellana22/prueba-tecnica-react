import { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import React from "react";
import UsersListService from "../services/UsersService";
import Page from "./Page";
import LoadingSpinner from "./LoadingSpinner";
import ErrorAlert from "./ErrorAlert";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import AddIcon from "@mui/icons-material/Add";

const UiState = {
  None: 0,
  Ready: 1,
  Loading: 2,
  ErrorLoading: 3,
};

const UsersListPage = () => {
  const navigate = useNavigate();
  const [uiState, setUiState] = useState(UiState.None);
  const [users, setUsers] = useState();

  const fetchAllUsers = async () => {
    try {
      const res = await UsersListService.getAll();
      setUsers(res.data);
      setUiState(UiState.Ready);
    } catch {
      setUiState(UiState.ErrorLoading);
    }
  };

  useEffect(() => {
    fetchAllUsers();
  }, []);

  const addUserClickHandler = () => {
    navigate("/create-user");
  };

  return (
    <Page
      title="Lista de usuarios"
      actionButtons={[
        {
          id: 1,
          icon: <AddIcon />,
          onClick: addUserClickHandler,
        },
      ]}
    >
      {uiState === UiState.Loading && <LoadingSpinner />}
      {uiState === UiState.ErrorLoading && (
        <ErrorAlert
          message="Error al cargar la lista de usuarios"
          onRetryClick={fetchAllUsers}
        />
      )}
      {uiState === UiState.Ready && (
        <List>
          {users &&
            users.map((user) => (
              <ListItem
                key={user.id}
                button
                component={Link}
                to={`/users/${user.id}`}
                style={{
                  textDecoration: "none",
                  width: "100%",
                  margin: "auto",
                }}
              >
                <ListItemText
                  primary={`${user.name}  ${user.lastName}`}
                  secondary={`${user.age} años`}
                />
              </ListItem>
            ))}
        </List>
      )}
    </Page>
  );
};

export default UsersListPage;
