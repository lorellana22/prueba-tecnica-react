import PropTypes from "prop-types";
import React from "react";
import Alert from "@mui/material/Alert";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";

const ErrorAlert = ({ message, onRetryClick }) => (
  <Box my={5}>
    <Alert
      severity="error"
      action={
        onRetryClick && 
        <Button 
        onClick={onRetryClick}>
            Reintentar
        </Button>
      }>
      {message}
    </Alert>
  </Box>
);
ErrorAlert.propTypes = {
  message: PropTypes.string.isRequired,
  onRetryClick: PropTypes.func.isRequired,
};

export default ErrorAlert;
