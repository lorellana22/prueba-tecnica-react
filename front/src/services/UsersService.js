import axios from "axios";

const baseUrl = process.env.REACT_APP_BASE_URL || "http://localhost:9091";
const usersEndpoint = "users";

const UsersService = {
  getAll: () => axios.get(`${baseUrl}/${usersEndpoint}/`),
  create: (user) => axios.post(`${baseUrl}/${usersEndpoint}/`, user),
  get: (user) => axios.put(`${baseUrl}/${usersEndpoint}/${user.id}`),
  update: (user) => axios.put(`${baseUrl}/${usersEndpoint}/${user.id}`, user),
  delete: (user) => axios.delete(`${baseUrl}/${usersEndpoint}/${user.id}`),
};
export default UsersService;
